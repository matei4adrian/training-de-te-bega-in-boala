const express = require("express"); // referinta catre lucrurile exportate de catre modul
// const connection = require("./models").connection;
const router = require("./routes"); // cu ajutorul acestei variabile, avem toate verbele HTTP (post() , put(), delete() etc.) => vezi folderul "routes"
const app = express(); // app-ul il folosim pentru a handle-ui request-urile

app.use(express.json()); // => functia express.json() analizeaza(parseaza) cererile(request-urile) primite ca JSON (se parseaza cu body-parser)

const port = 8080;

app.use("/api", router);

// In Express, everything is middleware. Internally, an Express app has a middleware stack, and calling use() 
// adds a new layer to the stack. Functions that define route handlers, like get() and post() 
// also add layers to the stack. Express executes the middleware stack in order, so the order in which you call use() matters.

// app.get("/reset", (req, res) => {
//   connection
//     .sync({ force: true })
//     .then(() => {
//       res
//         .status(201)
//         .send({ message: "Baza de date a fost resetata cu succes!" });
//     })
//     .catch((error) => {
//       console.log(error);
//       res.status(500).send({
//         message: "Resetarea bazei de date a esuat! Verificati consola!",
//       });
//     });
// });


app.use("/*", (req, res) => {
  res.status(200).send("Aplicatia ruleaza, dar aceasta ruta nu este definita!");
});

app.listen(port, () => {
  console.log(`Server-ul ruleaza pe portul ${port}!`);
}); // => listen ne ajuta sa legam conexiunea unui anumit host la port
