const Sequelize = require("sequelize");
const db = require("../config/db");
const CasaDeDiscuriModel = require("./casa-de-discuri");
const ManeaModel = require("./manea");

const CasaDeDiscuri = CasaDeDiscuriModel(db, Sequelize);
const Manea = ManeaModel(db, Sequelize);

CasaDeDiscuri.hasMany(Manea, {
  foreignKey: "casaDeDiscuriId",
  as: "Manele",
  onDelete: "CASCADE",
});
Manea.belongsTo(CasaDeDiscuri);

module.exports = {
  CasaDeDiscuri,
  Manea,
  connection: db,
};
