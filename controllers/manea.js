//Functia findByPk() => ne ajuta sa cautam o anumita instanta in functie de primary key (cheie primara)
//Functia save() (linia 76) => o folosim pentru a ne salva in baza de date modificarile pe care le-am facut intr-un obiect (se foloseste la rutele de update)

const CasaDeDiscuriDb = require("../models").CasaDeDiscuri;
const ManeaDb = require("../models").Manea;

const controller = {
  getAllManele: (req, res) => {
    ManeaDb.findAll()
      .then((manele) => {
        res.status(200).send(manele);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Eroare de server!" });
      });
  },
  getManeaById: (req, res) => {
    const { id } = req.params;
    if (!id) {
      res.status(400).send({ message: "Trebuie sa specifici id-ul!" });
    }

    ManeaDb.findByPk(id)
      .then((manea) => {
        if (manea) {
          res.status(200).send(manea);
        } else {
          res.status(404).send({ message: "Maneaua nu exista!" });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Eroare de server!" });
      });
  },
  addManea: (req, res) => {
    const { casaDeDiscuriId, denumire, artist } = req.body;
    CasaDeDiscuriDb.findByPk(casaDeDiscuriId)
      .then((casaDeDiscuri) => {
        if (casaDeDiscuri) {
          casaDeDiscuri
            .createManele({ denumire, artist })
            .then((manea) => {
              res.status(201).send(manea);
            })
            .catch((err) => {
              console.log(err);
              res.status(500).send({ message: "Eroare de server!" });
            });
        } else {
          res.status(404).send({ message: "Casa de discuri nu exista!" });
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Eroare de server!" });
      });
  },
  updateManea: (req, res) => {
    const { id } = req.params;
    if (!id) {
      res.status(400).send({ message: "Trebuie sa specifici id-ul!" });
    }

    const { denumire, artist } = req.body;
    if (!denumire && !artist) {
      res
        .status(400)
        .send({ message: "Trebuie sa specifici minim o modificare!" });
    } else {
      ManeaDb.findByPk(id)
        .then(async (manea) => {
          if (manea) {
            Object.assign(manea, req.body);
            await manea.save();
            res
              .status(202)
              .send({ message: "Maneaua a fost actualizata cu succes!" });
          } else {
            res.status(404).json({ message: "Maneaua nu exista!" });
          }
        })
        .catch((err) => {
          console.error(err);
          res.status(500).send({ message: "Eroare de server!" });
        });
    }
  },
};

module.exports = controller;
