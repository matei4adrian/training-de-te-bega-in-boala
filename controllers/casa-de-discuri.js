//Functia findAll() => ne ajuta sa cautam toate instantele (datele) din baza de date pentru un anumit model (tabela) 
//Functia create() => ne ajuta sa creeam o noua instanta in baza de date

const CasaDeDiscuriDb = require("../models").CasaDeDiscuri;
const ManeaDb = require("../models").Manea;

const controller = {
  getAllCaseDeDiscuri: (req, res) => {
    CasaDeDiscuriDb.findAll({ include: [{ model: ManeaDb, as: "Manele" }] })
      .then((caseDeDiscuri) => {
        res.status(200).send(caseDeDiscuri);
      })
      .catch((error) => {
        console.log(error);
        res.status(500).send({ message: "Eroare de server!" });
      });
  },
  addCasaDeDiscuri: (req, res) => {
    const { denumire } = req.body;
    CasaDeDiscuriDb.create({ denumire })
      .then((casaDeDiscuri) => {
        res.status(201).send(casaDeDiscuri);
      })
      .catch((err) => {
        console.log(err);
        res.status(500).send({ message: "Eroare de server!" });
      });
  },
};

module.exports = controller;
